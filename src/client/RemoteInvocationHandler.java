package client;

import client.annonation.RemoteReturn;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class RemoteInvocationHandler implements InvocationHandler {
    private int id;
    private String host;
    private int port;

    public RemoteInvocationHandler(String host, int port, int id) {
        this.host = host;
        this.port = port;
        this.id = id;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if(method.getName().equals("getRmiId"))
            return id;
        RMISocket socket = new RMISocket(host,port);
        socket.writeLine("INVOKE " + (args==null?0:args.length) + " " + method.getName() + " " + id +
                (method.getAnnotation(RemoteReturn.class)!=null?" REMOTE":" SERIALIZED"));
        if(args!=null)
            for(Object obj: args)
                socket.writeObject(obj);
        Object obj = null;
        if(method.getAnnotation(RemoteReturn.class)!=null) {
            Class type = method.getReturnType();
            obj = Proxy.newProxyInstance(type.getClassLoader(),new Class[]{type},
                                                new RemoteInvocationHandler(host,port,socket.readInt()));
        } else if(!method.getReturnType().getName().equals("void")) {
            obj = socket.readObject();
        }
        socket.close();
        return obj;
    }
}
