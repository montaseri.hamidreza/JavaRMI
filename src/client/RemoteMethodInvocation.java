package client;

import client.annonation.ImplementedBy;
import client.remoteInterface.RemoteInterface;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class RemoteMethodInvocation {
    private final String host;
    private final int port;

    public RemoteMethodInvocation(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public <T extends RemoteInterface> T newRemoteInstance(Class<T> a, Object... params) {
        int id;
        try {
            RMISocket socket = new RMISocket(host,port);
            String name = a.getAnnotation(ImplementedBy.class).value();
            socket.writeLine("NEW " + params.length + " " + name);
            for(Object obj: params)
                socket.writeObject(obj);
            id = socket.readInt();
            System.out.println(id);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return (T)Proxy.newProxyInstance(a.getClassLoader(), new Class[] {a}, new RemoteInvocationHandler(host,port,id));
    }

    public void releaseRemoteInstance(RemoteInterface remoteObject) {
        try {
            RMISocket socket = new RMISocket(host,port);
            InvocationHandler invocationHandler = Proxy.getInvocationHandler(remoteObject);
            Field idField  = invocationHandler.getClass().getDeclaredField("id");
            idField.setAccessible(true);
            int id = (int)idField.get(invocationHandler);
            socket.writeLine("RELEASE 0 "+id);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
