package client;

import client.remoteInterface.ARemote;
import client.remoteInterface.BRemote;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        String server = "";//server host name or ip
        int port = 9090;//server portSystem.out.println("connecting to server");
        RemoteMethodInvocation rmi = new RemoteMethodInvocation(server, port);
        System.out.println("creating A");
        ARemote a = rmi.newRemoteInstance(ARemote.class, 10);
        System.out.println("creating B");
        BRemote b1 = rmi.newRemoteInstance(BRemote.class);
        System.out.println("calling remote method addTo");
        BRemote b2 = a.addTo(b1);
        System.out.println(b2);
        System.out.println("releasing object on server");
        rmi.releaseRemoteInstance(a);
    }
}
