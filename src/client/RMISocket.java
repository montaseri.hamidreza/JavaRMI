package client;

import client.annonation.ImplementedBy;
import client.remoteInterface.RemoteInterface;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class RMISocket extends Socket{
    private final ObjectOutputStream objectOutputStream;
    private final ObjectInputStream objectInputStream;

    public RMISocket(String address, int port) throws IOException {
        super(address,port);
        this.objectInputStream = new ObjectInputStream(this.getInputStream());
        this.objectOutputStream = new ObjectOutputStream(this.getOutputStream());
        objectOutputStream.flush();
    }

    public void writeLine(String msg) throws IOException {
        objectOutputStream.writeChars(msg+"\n");
        objectOutputStream.flush();
    }

    public <T> void writeObject(T t) throws IOException {
        if(t instanceof RemoteInterface) {
            this.writeLine(t.getClass().getInterfaces()[0].getAnnotation(ImplementedBy.class).value() + " REMOTE");
            this.writeLine("" + ((RemoteInterface) t).getRmiId());
        } else if(t instanceof Serializable) {
            this.writeLine( t.getClass().getName() + " SERIALIZED");
            objectOutputStream.writeObject(t);
        }
        objectOutputStream.flush();
    }

    public int readInt() throws IOException {
        return this.objectInputStream.readInt();
    }

    public Object readObject() throws IOException, ClassNotFoundException {
        return objectInputStream.readObject();
    }
}