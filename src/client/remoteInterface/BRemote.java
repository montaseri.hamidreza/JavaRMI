package client.remoteInterface;

import client.annonation.ImplementedBy;

@ImplementedBy("server.remoteClass.B")
public interface BRemote extends RemoteInterface {

}
