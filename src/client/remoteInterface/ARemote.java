package client.remoteInterface;

import client.annonation.ImplementedBy;
import client.annonation.RemoteReturn;

@ImplementedBy("server.remoteClass.A")
public interface ARemote extends RemoteInterface {
    @RemoteReturn
    public BRemote addTo(BRemote b);
}
