package client.remoteInterface;

public interface RemoteInterface {
    boolean equals(Object obj);
    int hashCode();
    String toString();
}
