package server.remoteClass;

public class B {
    int j = 30;

    public B(Integer j) {
        System.out.println("On server: inside B's constructor with parameter");
        this.j = j;
    }

    public B() {
        System.out.println("On server: inside B's constructor");
    }

    @Override
    public String toString() {
        return "B(j=" + j + ")";
    }
}
