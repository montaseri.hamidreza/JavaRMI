package server.remoteClass;

public class A {
    int i = 0;
    public A(Integer i) {
        System.out.println("On server: inside A constructor");
        this.i = i;
    }

    public B addTo(B b) {
        System.out.println("On server: inside A.addTo method");
        return new B(i+b.j);
    }
}
