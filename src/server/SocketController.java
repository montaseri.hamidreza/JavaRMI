package server;

import server.remoteClass.A;

import java.io.*;
import java.net.Socket;
import java.util.StringTokenizer;

public class SocketController extends Thread {
    private final Socket socket;
    private final ObjectInputStream objectInputStream;
    private final ObjectOutputStream objectOutputStream;
    private final Server server;

    public SocketController(Server server, Socket socket) throws IOException {
        this.socket = socket;
        this.objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.flush();
        this.objectInputStream = new ObjectInputStream(socket.getInputStream());
        this.server = server;
    }

    @Override
    public void run() {
        try {
            //determine which type is the query
            /*  NEW <constructorParamsCount> <className>
                INVOKE <paramsCount> <methodName> <ID> REMOTE/SERIALIZABLE
                **********
                SERIALIZED <className>
                <serializedObject>
                REMOTE <className>
                <ID>
            */
            String line = readLine();
            System.out.println(line);
            StringTokenizer st = new StringTokenizer(line, " ");
            String queryType = st.nextToken();
            int paramCount = Integer.parseInt(st.nextToken());
            Object[] params = new Object[paramCount];
            Class[] types = new Class[paramCount];

            for (int i = 0; i < paramCount; i++) {
                String tmp = readLine();
                System.out.println(tmp);
                StringTokenizer inp = new StringTokenizer(tmp, " ");
                types[i] = Class.forName(inp.nextToken());
                if (inp.nextToken().equals("REMOTE"))
                    params[i] = server.getRemoteObject(Integer.parseInt(readLine()));
                else
                    params[i] = objectInputStream.readObject();
            }

            if (queryType.equals("NEW")) {
                int id = server.newRemoteObject(Class.forName(st.nextToken()), types, params);
                objectOutputStream.writeInt(id);
            } else if (queryType.equals("INVOKE")) {
                String methodName = st.nextToken();
                int id = Integer.parseInt(st.nextToken());

                if (st.nextToken().equals("REMOTE")) {
                    int resultId = server.invokeRemote(id, methodName, types, params);
                    objectOutputStream.writeInt(resultId);
                } else {
                    Object result = server.invokeSerializable(id, methodName, types, params);
                    objectOutputStream.writeObject(result);
                }

            } else if (queryType.equals("RELEASE"))
                server.releaseRemoteObject(Integer.parseInt(st.nextToken()));
            objectOutputStream.flush();
            System.out.println("done!");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String readLine() throws IOException {
        char c;
        String str = "";
        while ((c = objectInputStream.readChar()) != '\n')
            str += c;
        return str;
    }
}
