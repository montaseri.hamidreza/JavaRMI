package server;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Set;

public class Server extends Application {
    private static int port;
    private static ServerSocket serverSocket;
    private HashMap<Integer,Object> idToObject;
    private HashMap<Object,Integer> objectToId;
    private static int objectIdCounter = 1000000;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //TODO: GUI
        port = 9090;
        serverSocket = new ServerSocket(port);
        idToObject = new HashMap<>();
        objectToId = new HashMap<>();
        while(true) {
            new SocketController(this,serverSocket.accept()).start();
        }
    }

    public Object getRemoteObject(int id) {
        return idToObject.get(id);
    }

    public int newRemoteObject(Class<?> aClass, Class[] types, Object[] params) {
        try {
            Constructor constructor = (types.length>0?aClass.getDeclaredConstructor(types):aClass.getDeclaredConstructor());
            Object object = constructor.newInstance(params);
            objectToId.put(object,++objectIdCounter);
            idToObject.put(objectIdCounter,object);
            return objectIdCounter;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int invokeRemote(int id, String methodName, Class[] types, Object[] params) {
        Object result = invokeSerializable(id, methodName, types, params);
        if(null != objectToId.get(result)) {
            return objectToId.get(result);
        } else {
            objectToId.put(result,++objectIdCounter);
            idToObject.put(objectIdCounter,result);
            return objectIdCounter;
        }
    }

    public Object invokeSerializable(int id, String methodName, Class[] types, Object[] params) {
        Object object = idToObject.get(id);
        Method method;
        Object result = null;
        try {
            method = object.getClass().getMethod(methodName,types);
            result = null;
            if(params==null)
                result = method.invoke(object);
            else
                result = method.invoke(object,params);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void releaseRemoteObject(int id) {
        Object object = idToObject.get(id);
        objectToId.remove(object);
        idToObject.remove(id);
    }
}
